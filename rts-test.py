from typing import List, Dict

def aboveBelow(int_list: List, int_divide: int) -> Dict:
    these_above: int = 0
    these_below: int = 0
    ##
    for this_int in int_list:
        if this_int > int_divide:
            these_above += 1
        if this_int < int_divide:
            these_below += 1
    ##
    the_divide: Dict = {
        "above": these_above,
        "below": these_below
        }
    ##
    return the_divide

def stringRotation(original: str, rotate: int) -> str:
    string_length: int = len(original)
    ##
    to_the_back: str = original[:-rotate]
    to_the_front: str = original[(string_length - rotate):]
    ##
    return to_the_front + to_the_back

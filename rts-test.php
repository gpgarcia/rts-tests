<?php

function aboveBelow($int_list, $int_divide){
	$these_above = 0;
	$these_below = 0;

	foreach($int_list as $this_int){
		if($this_int > $int_divide){
			$these_above += 1;
		}
		if($this_int < $int_divide){
			$these_below += 1;
		}
	}

	$the_divide = array(
			"above" => $these_above,
			"below" => $these_below
		);

	return $the_divide;
}


function stringRotation($original, $rotate){
	$string_length = strlen($original);

	$to_the_back = substr($original, 0, ($string_length - $rotate));
	$to_the_front = substr($original, -$rotate, $rotate);

	return $to_the_front."".$to_the_back;
}